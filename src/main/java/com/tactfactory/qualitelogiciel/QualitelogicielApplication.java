package com.tactfactory.qualitelogiciel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QualitelogicielApplication {

    public static void main(String[] args) {
        SpringApplication.run(QualitelogicielApplication.class, args);
    }

}
