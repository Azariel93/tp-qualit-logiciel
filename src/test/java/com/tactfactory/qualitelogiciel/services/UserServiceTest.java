package com.tactfactory.qualitelogiciel.services;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import com.tactfactory.qualitelogiciel.QualitelogicielApplicationTests;
import com.tactfactory.qualitelogiciel.entities.Product;
import com.tactfactory.qualitelogiciel.entities.User;
import com.tactfactory.qualitelogiciel.mocks.repositories.MockitoProductRepository;
import com.tactfactory.qualitelogiciel.mocks.repositories.MockitoUserRepository;
import com.tactfactory.qualitelogiciel.repositories.ProductRepository;
import com.tactfactory.qualitelogiciel.repositories.UserRepository;

@ActiveProfiles("test")
@TestPropertySource(locations = { "classpath:application-test.properties" })
@SpringBootTest(classes = QualitelogicielApplicationTests.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private ProductRepository productRepository;

    private User entity;
    private Product entityProduct;

    @BeforeEach
    public void setUp() throws Exception {
      final MockitoUserRepository mock = new MockitoUserRepository(this.userRepository);
      mock.intialize();
      this.entity = mock.entity;
      final MockitoProductRepository mockProduct = new MockitoProductRepository(this.productRepository);
      mockProduct.intialize();
      this.entityProduct = mockProduct.entity;
    }

    /**
     * Test que l�insertion d�un �l�ment ajoute bien un enregistrement dans la base de donn�es
     */
    @Test
    public void TestInsertOne() {
        Long before = userRepository.count();
        userService.save(this.entity);
        Long after = userRepository.count();

        assertEquals(before + 1, after);
    }

    /**
     * Test que l�insertion d�un �l�ment n�a pas alt�rer les donn�es de l�objet sauvegard�
     */
    @Test
    public void TestInsertUser() {
        userService.save(this.entity);
        User userFetch = userRepository.getUserById(this.entity.getId());

        assertTrue(compare(this.entity, userFetch));
    }

    /**
     * Test que la mise � jour d�un �l�ment n�a pas alt�rer les donn�es de l�objet sauvegard�
     */
    @Test
    public void TestUpdateUser() {
        // Create User
        userService.save(this.entity);

        // Get user and set products
        User userFetch = userRepository.getUserById(this.entity.getId());
        userFetch.setFirstname("toto");

        // Update user and get id to check modifications
        userService.save(userFetch);
        User userFetchUpdated = userRepository.getUserById(this.entity.getId());

        assertTrue(compare(userFetch, userFetchUpdated));
    }

    /**
     * Test qu�un �l�ment est r�cup�r� avec les bonnes donn�es
     */
    @Test
    public void TestGetUser() {
        userRepository.save(this.entity);
        User userFetch = userService.getUserById(this.entity.getId());

        assertTrue(compare(this.entity, userFetch));
    }

    /**
     * Test qu�une liste est r�cup�r� avec les bonnes donn�es
     */
    @Test
    public void TestGetUsers() {
        List<User> users = new ArrayList<User>();
        User user1 = new User(null, "Tardif", "Dylan", new ArrayList<Product>());
        users.add(user1);
        User user2 = new User(null, "Gantois", "Emilien", new ArrayList<Product>());
        users.add(user2);

        for (User user : users) {
            userService.save(user);
        }

        List<User> usersFetch = userService.findAll();

        for (int i = 0; i < usersFetch.size(); i++) {
            assertTrue(compare(users.get(i), usersFetch.get(i)));
        }
    }

    /**
     * Test que la suppression d�un �l�ment d�cr�mente le nombre d�enregistrement pr�sent
     */
    @Test
    public void TestDeleteOne() {
        User userTemp = new User();
        userService.save(userTemp);
        Long before = userRepository.count();
        userService.delete(userTemp);
        Long after = userRepository.count();

        assertEquals(before - 1, after);
    }





    /**
     * Test que la suppression d�un �l�ment supprime bien le bon �l�ment
     */
    @Test
    public void TestDeleteUser() {
        List<User> users = new ArrayList<User>();
        User user1 = new User(1L, "Tardif", "Dylan", new ArrayList<Product>());
        users.add(user1);
        User user2 = new User(2L, "Gantois", "Emilien", new ArrayList<Product>());
        users.add(user2);

        for (User user : users) {
            userService.save(user);
        }
        userService.delete(user2);

        assertNull(userService.getUserById(user2.getId()));
    }





    /**
     * Cr�ation d�un nouvel �l�ment avec un sous �l�ment non existant en comptant
     */
    @Test
    public void TestCreaEntitePrimitive1() {
        Long productBefore = productRepository.count();

        List<Product> products = new ArrayList<Product>();
        products.add(new Product());
        User userBase = new User(null, "Tardif", "Dylan", products);

        userService.save(userBase).getId();
        Long productAfter = productRepository.count();

        assertEquals(productBefore, productAfter);
    }

    /**
     * Cr�ation d�un nouvel �l�ment avec un sous �l�ment non existant en regardant les valeur
     */
    @Test
    public void TestCreaEntitePrimitive2() {
        List<Product> products = new ArrayList<Product>();
        products.add(entityProduct);
        entity.setProducts(products);

        Long idUpdated = userService.save(entity).getId();
        assertTrue(compare(entity, userRepository.getUserById(idUpdated)));
    }

    /**
     * Cr�ation d�un nouvel �l�ment avec un sous �l�ment existant en comptant
     */
    @Test
    public void TestCreaEntitePrimitive3() {
        List<Product> products = new ArrayList<Product>();
        Product p1 = new Product(null, "product1", (float) 10);
        Long idP1 = productRepository.save(p1).getId();
        p1.setId(idP1);
        products.add(p1);
        User userBase = new User(null, "Tardif", "Dylan", products);

        Long before = userRepository.count();
        userService.save(userBase).getId();
        Long after = userRepository.count();

        assertEquals(before + 1, after);
    }

    /**
     * Cr�ation d�un nouvel �l�ment avec un sous �l�ment existant en regardant les valeur
     */
    @Test
    public void TestCreaEntitePrimitive4() {
        List<Product> products = new ArrayList<Product>();
        Product p1 = new Product(null, "product1", (float) 10);
        Long idP1 = productRepository.save(p1).getId();
        p1.setId(idP1);
        products.add(p1);
        User userBase = new User(null, "Tardif", "Dylan", products);

        Long idUpdated = userService.save(userBase).getId();
        User userFetch = userRepository.getUserById(idUpdated);

        assertTrue(compare(userBase, userFetch));
    }

    /**
     * Cr�ation d�un nouvel �l�ment avec un sous �l�ment � mettre � jour en comptant
     */
    @Test
    public void TestCreaEntitePrimitive5() {
        List<Product> products = new ArrayList<Product>();
        Product p1 = new Product(null, "product1", (float) 10);
        Long idP1 = productRepository.save(p1).getId();
        p1.setId(idP1);
        products.add(p1);
        products.get(0).setPrice((float) 12);
        User userBase = new User(null, "Tardif", "Dylan", products);

        Long before = userRepository.count();
        userService.save(userBase).getId();
        Long after = userRepository.count();

        assertEquals(before + 1, after);
    }

    /**
     * Cr�ation d�un nouvel �l�ment avec un sous �l�ment � mettre � jour en regardant les valeur
     */
    @Test
    public void TestCreaEntitePrimitive6() {
        List<Product> products = new ArrayList<Product>();
        Product p1 = new Product(null, "product1", (float) 10);
        Long idP1 = productRepository.save(p1).getId();
        p1.setId(idP1);
        products.add(p1);
        products.get(0).setPrice((float) 12);
        User userBase = new User(null, "Tardif", "Dylan", products);

        Long idUpdated = userService.save(userBase).getId();
        User userFetch = userRepository.getUserById(idUpdated);

        assertTrue(compare(userBase, userFetch));
    }

    /**
     * Cr�ation d�un nouvel �l�ment avec un sous �l�ment � enlever en comptant
     */
    @Test
    public void TestCreaEntitePrimitive7() {
        Long before = userRepository.count();

//      create user avec 3 produits en base
        List<Product> products = new ArrayList<Product>();
        Product p1 = new Product("product1", (float) 10);
        Long idP1 = productRepository.save(p1).getId();
        p1.setId(idP1);
        products.add(p1);

        Product p2 = new Product("product2", (float) 10);
        Long idP2 = productRepository.save(p2).getId();
        p2.setId(idP2);
        products.add(p2);

        Product p3 = new Product("product3", (float) 10);
        Long idP3 = productRepository.save(p3).getId();
        p3.setId(idP3);
        products.add(p3);

        User userBase = new User("Tardif", "Dylan", products);
        Long idCreate = userService.save(userBase).getId();

//      recup
        User userFetch = userRepository.getUserById(idCreate);

//      retire 1 produit
        userFetch.getProducts().remove(0);
        userFetch.setId(idCreate);

//      update
        userService.save(userFetch);

//      recup + check
        Long after = userRepository.count();

        assertEquals(before + 1, after);
    }

    /**
     * Cr�ation d�un nouvel �l�ment avec un sous �l�ment � enlever en regardant les valeur
     */
    @Test
    public void TestCreaEntitePrimitive8() {
//      create user avec 3 produits en base
        List<Product> products = new ArrayList<Product>();
        Product p1 = new Product(null, "product1", (float) 10);
        Long idP1 = productRepository.save(p1).getId();
        p1.setId(idP1);
        products.add(p1);

        Product p2 = new Product(null, "product2", (float) 10);
        Long idP2 = productRepository.save(p2).getId();
        p2.setId(idP2);
        products.add(p2);

        Product p3 = new Product(null, "product3", (float) 10);
        Long idP3 = productRepository.save(p3).getId();
        p3.setId(idP3);
        products.add(p3);

        User userBase = new User(null, "Tardif", "Dylan", products);
        Long idCreate = userService.save(userBase).getId();

//      recup
        User userFetch = userService.getUserById(idCreate);

//      retire 1 produit
        List<Product> productsUptaded = userFetch.getProducts();
        productsUptaded.remove(0);
        userFetch.setProducts(productsUptaded);

//      update
        userService.save(userFetch).getId();

//      recup + check
        User userUpdated = userRepository.getUserById(idCreate);

        assertTrue(compare(userUpdated, userFetch));
    }

    public Boolean compare(User user1, User user2) {
        boolean result = true;

        if (!user1.getId().equals(user2.getId())) {
            result = false;
            System.out.println("id: " + user1.getId() + " " + user2.getId());
        }
        if (!user1.getFirstname().equals(user2.getFirstname())) {
            result = false;
            System.out.println("firstname: " + user1.getFirstname() + " " + user2.getFirstname());
        }
        if (!user1.getLastname().equals(user2.getLastname())) {
            result = false;
            System.out.println("lastname: " + user1.getLastname() + " " + user2.getLastname());
        }
        if (!user1.getProducts().equals(user2.getProducts())) {
            result = false;
            System.out.println("products: " + user1.getProducts() + " " + user2.getProducts());
        }

        return result;
    }
}
