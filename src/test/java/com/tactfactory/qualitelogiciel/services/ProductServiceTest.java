package com.tactfactory.qualitelogiciel.services;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import com.tactfactory.qualitelogiciel.QualitelogicielApplicationTests;
import com.tactfactory.qualitelogiciel.entities.Product;
import com.tactfactory.qualitelogiciel.mocks.repositories.MockitoProductRepository;
import com.tactfactory.qualitelogiciel.repositories.ProductRepository;

@ActiveProfiles("test")
@TestPropertySource(locations = { "classpath:application-test.properties" })
@SpringBootTest(classes = QualitelogicielApplicationTests.class)
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @MockBean
    private ProductRepository productRepository;

    private Product entity;

    @BeforeEach
    public void setUp() throws Exception {
      final MockitoProductRepository mock = new MockitoProductRepository(this.productRepository);
      mock.intialize();
      this.entity = mock.entity;
    }

    /**
     * Test que l�insertion d�un �l�ment ajoute bien un enregistrement dans la base de donn�es
     */
    @Test
    public void TestInsertOne() {
        Long before = productRepository.count();
        productService.save(this.entity);
        Long after = productRepository.count();

        assertEquals(before + 1, after);
    }

    /**
     * Test que l�insertion d�un �l�ment n�a pas alt�rer les donn�es de l�objet sauvegard�
     */
    @Test
    public void TestInsertProduct() {
        productService.save(this.entity);
        Product productFetch = productRepository.getProductById(this.entity.getId());

        assertTrue(compare(this.entity, productFetch));
    }

    /**
     * Test que la mise � jour d�un �l�ment n�a pas alt�rer les donn�es de l�objet sauvegard�
     */
    @Test
    public void TestUpdateProduct() {
        // Create Product
        productService.save(this.entity);

        // Get Product and set new value
        Product productFetch = productRepository.getProductById(this.entity.getId());
        productFetch.setPrice((float) 20);

        // Update user and get id to check modifications
        productService.save(productFetch);
        Product productFetchUpdated = productRepository.getProductById(productFetch.getId());

        assertTrue(compare(productFetch, productFetchUpdated));
    }

    /**
     * Test qu�un �l�ment est r�cup�r� avec les bonnes donn�es
     */
    @Test
    public void TestGetProduct() {
        productRepository.save(this.entity);
        Product productFetch = productService.getProductById(this.entity.getId());

        assertTrue(compare(this.entity, productFetch));
    }

    /**
     * Test qu�une liste est r�cup�r� avec les bonnes donn�es
     */
    @Test
    public void TestGetProducts() {
        List<Product> products = new ArrayList<Product>();
        Product product1 = new Product(null, "product1", (float) 10);
        products.add(product1);
        Product product2 = new Product(null, "product2", (float) 10);
        products.add(product2);

        for (Product product : products) {
            productService.save(product);
        }

        List<Product> productsFetch = productService.findAll();

        for (int i = 0; i < productsFetch.size(); i++) {
            assertTrue(compare(products.get(i), productsFetch.get(i)));
        }
    }

    /**
     * Test que la suppression d�un �l�ment d�cr�mente le nombre d�enregistrement pr�sent
     */
    @Test
    public void TestDeleteOne() {
        Product productTemp = new Product();
        productService.save(productTemp);
        Long before = productRepository.count();
        productService.delete(productTemp);
        Long after = productRepository.count();

        assertEquals(before - 1, after);
    }





    /**
     * Test que la suppression d�un �l�ment supprime bien le bon �l�ment
     */
    @Test
    public void TestDeleteProduct() {
        Product productBase = new Product(null, "product1", (float) 10);
        Long id = productRepository.save(productBase).getId();
        productService.delete(productBase);

        Product deletedProduct = productService.getProductById(id);
        assertNull(deletedProduct);
    }

    public Boolean compare(Product product1, Product product2) {
        boolean result = true;

        if (!product1.getId().equals(product2.getId())) {
            result = false;
            System.out.println("id: " + product1.getId() + " " + product2.getId());
        }
        if (!product1.getName().equals(product2.getName())) {
            result = false;
            System.out.println("name: " + product1.getName() + " " + product2.getName());
        }
        if (!product1.getPrice().equals(product2.getPrice())) {
            result = false;
            System.out.println("price: " + product1.getPrice() + " " + product2.getPrice());
        }

        return result;
    }
}
