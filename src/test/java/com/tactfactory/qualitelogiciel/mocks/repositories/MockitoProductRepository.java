package com.tactfactory.qualitelogiciel.mocks.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.tactfactory.qualitelogiciel.entities.Product;
import com.tactfactory.qualitelogiciel.repositories.ProductRepository;

public class MockitoProductRepository {
    protected final ProductRepository repository;

    public Product entity;

    public Long number;

    public Optional<Product> resultOptional;

    public List<Product> products = new ArrayList<Product>();

    public MockitoProductRepository(ProductRepository repository) {
        this.repository = repository;
        this.number = 1L;

        this.entity = new Product();
        this.entity.setName("p1");
        this.entity.setPrice(10f);

        this.entity.setId(this.number);
    }

    public void intialize() {

        Mockito.when(this.repository.count()).thenAnswer(new Answer<Long>() {
            @Override
            public Long answer(InvocationOnMock invocation) throws Throwable {
                return (long) MockitoProductRepository.this.products.size();
            }
        });

        Mockito.when(this.repository.save(ArgumentMatchers.any())).thenAnswer(new Answer<Product>() {
            @Override
            public Product answer(InvocationOnMock invocation) throws Throwable {
                Boolean update = false;
                Product productEntity = invocation.getArgument(0);
                for (Product product : MockitoProductRepository.this.products) {
                    if (product.getId() == productEntity.getId()) {
                        update = true;
                        int index = MockitoProductRepository.this.products.indexOf(product);
                        MockitoProductRepository.this.products.get(index).setName(productEntity.getName());
                        MockitoProductRepository.this.products.get(index).setPrice(productEntity.getPrice());
                    }
                }
                if (update == false) {
                    MockitoProductRepository.this.number++;
                    productEntity.setId(MockitoProductRepository.this.number);
                    MockitoProductRepository.this.products.add(productEntity);
                }
                return productEntity;
            }
        });

        Mockito.when(this.repository.getProductById(ArgumentMatchers.anyLong())).thenAnswer(new Answer<Product>() {
            @Override
            public Product answer(InvocationOnMock invocation) throws Throwable {
                Long id = invocation.getArgument(0);
                Product productFetched = new Product();
                for (Product product : products) {
                    if (product.getId() == id) {
                        productFetched = product;
                    }
                }
                return productFetched;
            }
        });

        Mockito.when(this.repository.findAll()).thenAnswer(new Answer<List<Product>>() {
            @Override
            public List<Product> answer(InvocationOnMock invocation) throws Throwable {
                return MockitoProductRepository.this.products;
            }
        });

        Mockito.doAnswer((i) -> {
            if (i.getMethod().getName() == "delete") {
                MockitoProductRepository.this.products
                        .remove(products.stream().filter(product -> product.getId() == (long) i.<Product>getArgument(0).getId())
                                .findFirst().orElse(null));
                MockitoProductRepository.this.number --;
            }
            return null;
        }).when(this.repository).delete(ArgumentMatchers.any());
    }
}
