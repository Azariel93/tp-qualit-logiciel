package com.tactfactory.qualitelogiciel.mocks.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.tactfactory.qualitelogiciel.entities.Product;
import com.tactfactory.qualitelogiciel.entities.User;
import com.tactfactory.qualitelogiciel.repositories.UserRepository;

public class MockitoUserRepository {
    protected final UserRepository repository;

    public User entity;

    public Long number;

    public Optional<User> resultOptional;

    public List<User> users = new ArrayList<User>();

    public MockitoUserRepository(UserRepository repository) {
        this.repository = repository;
        this.number = 1L;

        this.entity = new User();
        this.entity.setFirstname("f1");
        this.entity.setLastname("l1");
        this.entity.setProducts(new ArrayList<Product>());

        this.entity.setId(this.number);
    }

    public void intialize() {

        Mockito.when(this.repository.count()).thenAnswer(new Answer<Long>() {
            @Override
            public Long answer(InvocationOnMock invocation) throws Throwable {
                return (long) MockitoUserRepository.this.users.size();
            }
        });

        Mockito.when(this.repository.save(ArgumentMatchers.any())).thenAnswer(new Answer<User>() {
            @Override
            public User answer(InvocationOnMock invocation) throws Throwable {
                Boolean update = false;
                User userEntity = invocation.getArgument(0);
                for (User user : MockitoUserRepository.this.users) {
                    if (user.getId() == userEntity.getId()) {
                        update = true;
                        int index = MockitoUserRepository.this.users.indexOf(user);
                        MockitoUserRepository.this.users.get(index).setFirstname(userEntity.getFirstname());
                        MockitoUserRepository.this.users.get(index).setLastname(userEntity.getLastname());
                        MockitoUserRepository.this.users.get(index).setProducts(userEntity.getProducts());
                    }
                }
                if (update == false) {
                    MockitoUserRepository.this.number++;
                    userEntity.setId(MockitoUserRepository.this.number);
                    MockitoUserRepository.this.users.add(userEntity);
                }
                return userEntity;
            }
        });

        Mockito.when(this.repository.getUserById(ArgumentMatchers.anyLong())).thenAnswer(new Answer<User>() {
            @Override
            public User answer(InvocationOnMock invocation) throws Throwable {
                Long id = invocation.getArgument(0);
                User userFetched = new User();
                for (User user : users) {
                    if (user.getId() == id) {
                        userFetched = user;
                    }
                }
                return userFetched;
            }
        });

        Mockito.when(this.repository.findAll()).thenAnswer(new Answer<List<User>>() {
            @Override
            public List<User> answer(InvocationOnMock invocation) throws Throwable {
                return MockitoUserRepository.this.users;
            }
        });

        Mockito.doAnswer((i) -> {
            if (i.getMethod().getName() == "delete") {
                MockitoUserRepository.this.users
                        .remove(users.stream().filter(user -> user.getId() == (long) i.<User>getArgument(0).getId())
                                .findFirst().orElse(null));
                MockitoUserRepository.this.number --;
            }
            return null;
        }).when(this.repository).delete(ArgumentMatchers.any());
    }
}
